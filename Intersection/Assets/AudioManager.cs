﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    [SerializeField] List<SoundEffect> totalEffects = new List<SoundEffect>();
    private AudioSource AuS;
    [System.Serializable]
    public class SoundEffect
    {
        public AudioClip audioClip;
        public string name;
    }
    public void PlayEffect(string effect)
    {
        foreach(SoundEffect e in totalEffects)
        {
            if(e.name == effect)
            {
                AuS.clip = e.audioClip;
                AuS.Play();
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
        AuS = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
