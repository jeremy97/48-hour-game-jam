﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
public class CameraEffects : MonoBehaviour
{

    [SerializeField] private float m_EffectsAmount = 0.7f;
    [SerializeField] private float m_DecreaseFactor = 1.0f;
    [SerializeField] private float m_MaxChromaticAbberation = 1.0f;
    private float m_EffectsDuration;
    private PostProcessingProfile m_PostprocessingProfile;
    private Camera m_Camera;

    private void Start()
    {
        m_Camera = GetComponent<Camera>();
        m_PostprocessingProfile = GetComponent<PostProcessingBehaviour>().profile;
    }

    public void SetDuration(float aDuration)
    {
        m_EffectsDuration = aDuration;
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        if (m_EffectsDuration > 0)
        {
            m_Camera.fieldOfView = Mathf.Lerp(m_Camera.fieldOfView, 100, m_EffectsAmount * Time.deltaTime);
            ChromaticAberrationModel.Settings chromaticAberration = m_PostprocessingProfile.chromaticAberration.settings;
            chromaticAberration.intensity = Mathf.Lerp(chromaticAberration.intensity, m_MaxChromaticAbberation, m_EffectsAmount * Time.deltaTime);
            m_PostprocessingProfile.chromaticAberration.settings = chromaticAberration;

             m_EffectsDuration -= Time.deltaTime * m_DecreaseFactor;
        }
        else
        {
            m_Camera.fieldOfView = Mathf.Lerp(m_Camera.fieldOfView, 80, m_EffectsAmount * Time.deltaTime);
            ChromaticAberrationModel.Settings chromaticAberration = m_PostprocessingProfile.chromaticAberration.settings;
            chromaticAberration.intensity = Mathf.Lerp(chromaticAberration.intensity, .1f, m_EffectsAmount * Time.deltaTime);
            m_PostprocessingProfile.chromaticAberration.settings = chromaticAberration;

            m_EffectsDuration = 0;
        }
    }
}
