﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuPlayerInput : MonoBehaviour
{


    [SerializeField] private Image playerOneButton, playerTwoButton, playerThreeButton;
    //[SerializeField] private Image playerOnePanel, playerTwoPanel, playerThreePanel;
    [SerializeField] List<GameObject> objectsToStart = new List<GameObject>();
    [SerializeField] List<GameObject> objectsToDeactivate = new List<GameObject>();
    [SerializeField] List<GameObject> objectsToEnd = new List<GameObject>();
    [SerializeField] TMPro.TMP_Text timer1, timer2, timer3;
    [SerializeField] CameraFade cameraFade;
    public List<Animator> playerAnims = new List<Animator>();
    private InputFrame player1 = new InputFrame();
    private InputFrame player2 = new InputFrame();
    private InputFrame player3 = new InputFrame();
    [SerializeField] private Image p1Check, p1X, p2Check, p2X, p3Check, p3X;
    [SerializeField] private Image p1CheckR, p1XR, p2CheckR, p2XR, p3CheckR, p3XR;
    [SerializeField] private GameObject sphereMap, donutMap;

    private bool playerOneReady, playerTwoReady, playerThreeReady;
    Color aWhite = Color.white;
    Color aGreen = Color.green;
    private bool gameStarted;
    private int time;
    private bool input = false;

    private Vector3 baseScale;
    // Start is called before the first frame update
    void Start()
    {
        baseScale = playerOneButton.transform.localScale;
        //blue 0 green 1 red 2
        aWhite.a = .2f;
        aGreen.a = .2f;
        if(PlayerVariableHolder.instance.playerVsColor[0] == 0)
        {
            p1Check.color = Color.red;
            p1X.color = Color.green;
            if(p1CheckR != null)
            {
                p1CheckR.color = Color.red;
                p1XR.color = Color.green;
            }
        }
        else if(PlayerVariableHolder.instance.playerVsColor[0] == 1)
        {
            p1Check.color = Color.blue;
            p1X.color = Color.red;
            if (p1CheckR != null)
            {
                p1CheckR.color = Color.blue;
                p1XR.color = Color.red;
            }
        }
        else
        {
            p1Check.color = Color.green;
            p1X.color = Color.blue;
            if (p1CheckR != null)
            {
                p1CheckR.color = Color.green;
                p1XR.color = Color.blue;
            }
        }
        if (PlayerVariableHolder.instance.playerVsColor[1] == 0)
        {
            p2Check.color = Color.red;
            p2X.color = Color.green;
            if (p1CheckR != null)
            {
                p2CheckR.color = Color.red;
                p2XR.color = Color.green;
            }
        }
        else if (PlayerVariableHolder.instance.playerVsColor[1] == 1)
        {
            p2Check.color = Color.blue;
            p2X.color = Color.red;
            if (p1CheckR != null)
            {
                p2CheckR.color = Color.blue;
                p2XR.color = Color.red;
            }
        }
        else
        {
            p2Check.color = Color.green;
            p2X.color = Color.blue;
            if (p1CheckR != null)
            {
                p2CheckR.color = Color.green;
                p2XR.color = Color.blue;
            }
        }
        if (PlayerVariableHolder.instance.playerVsColor[2] == 0)
        {
            p3Check.color = Color.red;
            p3X.color = Color.green;
            if (p1CheckR != null)
            {
                p3CheckR.color = Color.red;
                p3XR.color = Color.green;
            }
        }
        else if (PlayerVariableHolder.instance.playerVsColor[2] == 1)
        {
            p3Check.color = Color.blue;
            p3X.color = Color.red;
            if (p1CheckR != null)
            {
                p3CheckR.color = Color.blue;
                p3XR.color = Color.red;
            }
        }
        else
        {
            p3Check.color = Color.green;
            p3X.color = Color.blue;
            if (p1CheckR != null)
            {
                p3CheckR.color = Color.green;
                p3XR.color = Color.blue;
            }
        }
        // playerOnePanel.color = aWhite;
        // playerTwoPanel.color = aWhite;
        //playerThreePanel.color = aWhite;
    }
    // Update is called once per frame
    void Update()
    {
        player1 = InputFrame.FetchInput(1);
        player2 = InputFrame.FetchInput(2);
        player3 = InputFrame.FetchInput(3);
        if (player1.Fire)
        {
            input = true;

        }
        else
        {
            input = false;
        }
        if (!gameStarted)
        {
            if (player1.Fire)
            {
                Debug.Log("Fire1");
                if (!playerOneReady)
                {
                    AudioManager.instance.PlayEffect("Button_Select");
                    playerOneButton.color = Color.green;
                    //playerOnePanel.color = aGreen;
                    iTween.Stop(playerOneButton.gameObject);
                    playerOneButton.transform.localScale = baseScale;
                    iTween.PunchScale(playerOneButton.gameObject, baseScale * 1.2f, .5f);
                    playerOneReady = true;
                    playerAnims[0].SetTrigger("Ready");
                }
                else
                {
                    AudioManager.instance.PlayEffect("Button_Deselect");
                    // playerOnePanel.color = aWhite;
                    iTween.Stop(playerOneButton.gameObject);
                    playerOneButton.transform.localScale = baseScale;
                    iTween.PunchScale(playerOneButton.gameObject, baseScale * 1.2f, .5f);
                    playerOneButton.color = Color.white;
                    playerOneReady = false;
                }
            }
 
            if (player2.Fire)
            {
                if (!playerTwoReady)
                {
                    AudioManager.instance.PlayEffect("Button_Select");
                    //playerTwoPanel.color = aGreen;
                    iTween.Stop(playerTwoButton.gameObject);
                    playerTwoButton.transform.localScale = baseScale;
                    iTween.PunchScale(playerTwoButton.gameObject, baseScale * 1.2f, .5f);
                    playerTwoButton.color = Color.green;
                    playerTwoReady = true;
                    playerAnims[1].SetTrigger("Ready");

                }
                else
                {
                    AudioManager.instance.PlayEffect("Button_Deselect");
                    //playerTwoPanel.color = aWhite;
                    iTween.Stop(playerTwoButton.gameObject);
                    playerTwoButton.transform.localScale = baseScale;
                    iTween.PunchScale(playerTwoButton.gameObject, baseScale * 1.2f, .5f);
                    playerTwoButton.color = Color.white;
                    playerTwoReady = false;
                }
            }
            if (player3.Fire)
            {
                if (!playerThreeReady)
                {
                    AudioManager.instance.PlayEffect("Button_Select");
                    iTween.Stop(playerThreeButton.gameObject);
                    playerThreeButton.transform.localScale = baseScale;
                    iTween.PunchScale(playerThreeButton.gameObject, baseScale * 1.2f, .5f);
                    // playerThreePanel.color = aGreen;
                    playerThreeButton.color = Color.green;
                    playerThreeReady = true;
                    playerAnims[2].SetTrigger("Ready");

                }
                else
                {
                    AudioManager.instance.PlayEffect("Button_Deselect");
                    //  playerThreePanel.color = aWhite;
                    iTween.Stop(playerThreeButton.gameObject);
                    playerThreeButton.transform.localScale = baseScale;
                    iTween.PunchScale(playerThreeButton.gameObject, baseScale * 1.2f, .5f);
                    playerThreeButton.color = Color.white;
                    playerThreeReady = false;
                }
            }
        }
        if (playerOneReady && playerTwoReady && playerThreeReady)
        {
            if(!gameStarted)
                StartCoroutine( StartGame());
        }
    }
    IEnumerator StartGame()
    {
        gameStarted = true;
        cameraFade.FadeBoth(Color.black, 2.2f);
        yield return new WaitForSeconds(2.2f);
        sphereMap.SetActive(false);
        donutMap.SetActive(true);
        MusicManager.instance.NewMusic();
        foreach (GameObject obj in objectsToStart)
        {
            obj.SetActive(true);
        }
        foreach(GameObject obj in objectsToDeactivate)
        {
            obj.SetActive(false);
        }
        StartCoroutine(EndGame());
    }
    IEnumerator EndGame()
    {
        int timerN = 60;
        for(int i = 0; i < timerN; i++)
        {
            yield return new WaitForSeconds(1);
            time++;
            timer1.text = "Time: " + (timerN- time).ToString();
            timer2.text = "Time: " + (timerN - time).ToString();
            timer3.text = "Time: " + (timerN - time).ToString();

        }
        cameraFade.FadeBoth(Color.black, 2.2f);
        yield return new WaitForSeconds(2.2f);
        sphereMap.SetActive(true);
        donutMap.SetActive(false);
        MusicManager.instance.NewMusic();
        foreach (GameObject obj in objectsToStart)
        {
            obj.SetActive(false);
        }
        foreach (GameObject obj in objectsToEnd)
        {
            obj.SetActive(true);
        }
        while (!input)
        {
            yield return new WaitForEndOfFrame();
        }
        SceneManager.LoadScene(0);
    }
}
