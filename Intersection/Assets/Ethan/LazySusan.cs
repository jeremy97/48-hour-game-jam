﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazySusan : MonoBehaviour
{
    [SerializeField] float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float rand = Random.Range(.1f, .3f);

        transform.Rotate(new Vector3(0, speed * rand, 0));
    }
}
