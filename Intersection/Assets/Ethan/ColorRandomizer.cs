﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public class ColorRandomizer : MonoBehaviour
{
    [SerializeField] private List<RawImage> textures = new List<RawImage>();
    [SerializeField] private List<PlayerTempClass> players = new List<PlayerTempClass>();
    [SerializeField] private List<Animator> playerAnims = new List<Animator>();
    [SerializeField] private MenuPlayerInput menuPlayerInput;
    // Start is called before the first frame update
    [System.Serializable]
    public class PlayerTempClass
    {
        public Material mat;
        public int number;
    }
    void Start()
    {
        menuPlayerInput = FindObjectOfType<MenuPlayerInput>();
        for (int i = 0; i < 3; i++)
        {
            int rand = Random.Range(0, players.Count);
            PlayerVariableHolder.instance.colorVsPlayer[players[rand].number] = i;
            PlayerVariableHolder.instance.playerVsColor[i] = players[rand].number;
            textures[i].material = players[rand].mat;
            menuPlayerInput.playerAnims[i] = playerAnims[rand];
            playerAnims.RemoveAt(rand);
            players.RemoveAt(rand);

        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
