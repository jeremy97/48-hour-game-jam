﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trail : MonoBehaviour
{
    [SerializeField] private GameObject poop;
    [SerializeField] float poopSpeed;
    [SerializeField] private PoopType thisType;
    [SerializeField] private shaderTest st;
    [SerializeField] private GameObject positiveInt, negativeInt;
    [SerializeField] private List<CameraController> cams = new List<CameraController>();
    [SerializeField] private GameObject myShield;
    private bool hasShield;
    public CameraFade myCamFade;
    public CameraShake myCamShake;
    public CameraEffects myCamFX;
    bool cool;
    private Vector3 startPosition;
    public bool dead;
    public enum PoopType
    {
        red,
        green,
        blue
    }
    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        myShield.SetActive(false);
        myCamShake = cams[GetComponent<PlayerController>().PlayerNumber - 1].GetComponentInChildren<CameraShake>();
        myCamFX = cams[GetComponent<PlayerController>().PlayerNumber - 1].GetComponentInChildren<CameraEffects>();
        StartCoroutine(PoopCubes());
        st.camNoi = cams[GetComponent<PlayerController>().PlayerNumber - 1].GetComponentInChildren<cameraNoise>();
        myCamFade = myCamShake.myCamFade;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if( collision.gameObject.tag == "DeathWall" && !dead)
        StartCoroutine(Death());
    }
    IEnumerator Death()
    {
        AudioManager.instance.PlayEffect("Explosion");
        myCamFade.FadeBoth(Color.black, 2f);
        dead = true;
        myShield.SetActive(false);
        hasShield = false;
        st.lingerSpeed = .0005f;
st.wasHit = true;
        yield return new WaitForSeconds(2f);
        PlayerController pc = GetComponent<PlayerController>();
        pc.CurrentMaxSpeedRate = .4f;
        pc.m_currentSpeedRate = 0;
        transform.position = startPosition;
        dead = false;
        st.lingerSpeed = 5f;
        yield return new WaitForSeconds(1f);

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Powerup")
        {
            AudioManager.instance.PlayEffect("Glass");
            myShield.SetActive(true);
            hasShield = true;
            PickupSpawn.instance.DestroyPick(other.transform.parent);
            Destroy(other.gameObject);
        }
        if (other.tag == "Poop" && !cool)
        {

            PoopType hitType = other.gameObject.GetComponent<poopScript>().thisPoopType;
            if(thisType == PoopType.red)
            {
                if(hitType == PoopType.green)
                {
                    Debug.Log("Red win green");
                    //ScoreManager.instance.AddScore(gameObject.GetComponent<PlayerController>().PlayerNumber, 1);
                    StartCoroutine(Cooldown());
                    ChangeSpeed(.1f, .3f);
                    Instantiate(positiveInt, gameObject.transform.position, gameObject.transform.rotation);
                    AudioManager.instance.PlayEffect("Speed_Up");
                    myCamFX.SetDuration(1f);
                }
                else if(hitType == PoopType.blue)
                {
                    if (hasShield)
                    {
                        myShield.SetActive(false);
                        hasShield = false;
                        AudioManager.instance.PlayEffect("Shield");
                        StartCoroutine(Cooldown());
                    }
                    else
                    {
                        Debug.Log("Red loose blue");
                        //ScoreManager.instance.AddScore(gameObject.GetComponent<PlayerController>().PlayerNumber, -1);
                        AudioManager.instance.PlayEffect("Speed_Down");
                        StartCoroutine(Cooldown());
                        ChangeSpeed(-.1f, -.5f);
                        st.wasHit = true;
                        Instantiate(negativeInt, gameObject.transform.position, gameObject.transform.rotation);
                        myCamShake.SetDuration(.5f);
                    }
                }
            }
            else if(thisType == PoopType.green)
            {
                if (hitType == PoopType.blue)
                {
                    StartCoroutine(Cooldown());
                    ChangeSpeed(.1f, .3f);
                    Instantiate(positiveInt, gameObject.transform.position, gameObject.transform.rotation);

                    Debug.Log("Green win blue");
                    //ScoreManager.instance.AddScore(gameObject.GetComponent<PlayerController>().PlayerNumber, 1);
                    AudioManager.instance.PlayEffect("Speed_Up");
                    myCamFX.SetDuration(1f);

                }
                else if (hitType == PoopType.red)
                {
                    if (hasShield)
                    {
                        myShield.SetActive(false);
                        hasShield = false;
                        StartCoroutine(Cooldown());
                        AudioManager.instance.PlayEffect("Shield");
                    }
                    else
                    {
                        Debug.Log("Green lose red");
                        //ScoreManager.instance.AddScore(gameObject.GetComponent<PlayerController>().PlayerNumber, -1);
                        AudioManager.instance.PlayEffect("Speed_Down");
                        StartCoroutine(Cooldown());
                        ChangeSpeed(-.1f, -.5f);
                        st.wasHit = true;
                        Instantiate(negativeInt, gameObject.transform.position, gameObject.transform.rotation);
                        myCamShake.SetDuration(.5f);
                    }
                }
            }
            else if(thisType == PoopType.blue)
            {
                if (hitType == PoopType.red)
                {
                    StartCoroutine(Cooldown());
                    ChangeSpeed(.1f, .3f);
                    Debug.Log("blue win red");
                    //ScoreManager.instance.AddScore(gameObject.GetComponent<PlayerController>().PlayerNumber, 1);
                    AudioManager.instance.PlayEffect("Speed_Up");
                    Instantiate(positiveInt, gameObject.transform.position, gameObject.transform.rotation);
                    myCamFX.SetDuration(1f);

                }
                else if (hitType == PoopType.green )
                {
                    if (hasShield)
                    {
                        myShield.SetActive(false);
                        hasShield = false;
                        StartCoroutine(Cooldown());
                        AudioManager.instance.PlayEffect("Shield");
                    }
                    else
                    {
                        StartCoroutine(Cooldown());

                        ChangeSpeed(-.1f, -.5f);
                        Debug.Log("blue loose green");
                        // ScoreManager.instance.AddScore(gameObject.GetComponent<PlayerController>().PlayerNumber, -1);
                        AudioManager.instance.PlayEffect("Speed_Down");
                        st.wasHit = true;
                        Instantiate(negativeInt, gameObject.transform.position, gameObject.transform.rotation);
                        myCamShake.SetDuration(.5f);
                    }
                }
            }
        }
    }
    void ChangeSpeed(float max, float current)
    {
        GetComponent<PlayerController>().m_currentSpeedRate += current;
        GetComponent<PlayerController>().CurrentMaxSpeedRate += max;
        GetComponent<PlayerController>().CurrentMaxSpeedRate = Mathf.Clamp(GetComponent<PlayerController>().CurrentMaxSpeedRate, .1f, 1f);
        GetComponent<PlayerController>().m_currentSpeedRate = Mathf.Clamp(GetComponent<PlayerController>().m_currentSpeedRate, .1f, 1f);
    }
    IEnumerator Cooldown()
    {
        cool = true;
        yield return new WaitForSeconds(1);
        cool = false;
    }
    IEnumerator PoopCubes()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            if (GetComponent<PlayerController>().GetCurrentSpeedRate() > .01f)
            {
                yield return new WaitForSeconds(poopSpeed / (GetComponent<PlayerController>().GetCurrentSpeedRate() * 40));
                GameObject newPoop = Instantiate(poop, gameObject.transform.GetChild(0).position, gameObject.transform.rotation);
                newPoop.GetComponent<poopScript>().thisPoopType = thisType;

            }
        }

    }
}
