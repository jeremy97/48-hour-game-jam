﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVariableHolder : MonoBehaviour
{
    public Dictionary<int, int> colorVsPlayer = new Dictionary<int, int>();
    public Dictionary<int, int> playerVsColor = new Dictionary<int, int>();
    public static PlayerVariableHolder instance;
    // Start is called before the first frame update
    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else 
        {
            Destroy(this);
        }   
    }

    // Update is called once per frame
    void Update()
    {
    }
}
