﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public static MusicManager instance;
    [SerializeField] private AudioClip newMusic;
    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    public void NewMusic()
    {
        GetComponent<AudioSource>().clip = newMusic;
        GetComponent<AudioSource>().Play();
    }
    // Update is called once per frame
    void Update()
    {
    }
}
