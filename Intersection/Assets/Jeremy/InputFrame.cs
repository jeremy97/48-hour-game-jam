﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public struct InputFrame
{
    public static InputFrame FetchInput(int playerNumber)
    {
        Player p = ReInput.players.GetPlayer(playerNumber - 1);
        InputFrame frame = new InputFrame();
        frame.MoveVector = new Vector2(p.GetAxis("Steering"), p.GetAxis("Throttle")); //Input.GetAxisRaw("Horizontal" + playerNumber.ToString()), Input.GetAxisRaw("Trigger" + playerNumber.ToString()));
        frame.Fire = p.GetButtonDown("Accept"); //Input.GetButtonDown("Fire" + playerNumber.ToString());

        return frame;
    }
    public bool Fire;
    public Vector2 MoveVector;
}
