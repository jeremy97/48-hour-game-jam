﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVisual : MonoBehaviour
{
    
    public Transform m_target;
    [SerializeField]
    private float m_maxDistance;
    [SerializeField]
    private float m_maxAngle;
    [SerializeField]
    private float m_posLerpMult;
    [SerializeField]
    private float m_rotLerpMult;

    private void Update()
    {
        Vector3 posDiff = transform.position - m_target.position;
        float posLerp = posDiff.sqrMagnitude / (m_maxDistance * m_maxDistance) * Time.deltaTime * m_posLerpMult;

        float rotDiff = Quaternion.Angle(transform.rotation, m_target.rotation);
        float rotLerp = rotDiff / m_maxAngle * Time.deltaTime * m_rotLerpMult;

        transform.position = Vector3.Lerp(transform.position, m_target.position, posLerp);
        transform.rotation = Quaternion.Slerp(transform.rotation, m_target.rotation, rotLerp);
    }
}
