﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CameraController : MonoBehaviour
{
    public Transform m_Target;
    [SerializeField]
    private float m_MaxDistance;
    [SerializeField]
    private float m_LerpRate;
    [SerializeField]
    private int m_CameraNumber;
    [SerializeField] private List<Transform> transforms = new List<Transform>();
    [SerializeField] private List<Image> borders = new List<Image>();
    [SerializeField] private List<Color> colors = new List<Color>();
    private void Awake()
    {
        if (PlayerVariableHolder.instance != null)
        {
            m_Target = transforms[PlayerVariableHolder.instance.playerVsColor[m_CameraNumber - 1]];
        }
        else
        {
            Debug.Log("PlayerVariable holder is null");
        }
    }
    // Update is called once per frame
    void LateUpdate()
    {
        float sqrDist = (m_Target.position - transform.position).sqrMagnitude;

        float lerpScale = sqrDist / (m_MaxDistance * m_MaxDistance) * m_LerpRate;

        transform.position = Vector3.Lerp(transform.position, m_Target.position, lerpScale);
        transform.rotation = Quaternion.Slerp(transform.rotation, m_Target.rotation, lerpScale);
    }
}
