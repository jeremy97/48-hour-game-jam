﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [HideInInspector]
    public float CurrentMaxSpeedRate;
    [Range(1, 3)]
    public int PlayerNumber;
    [SerializeField]
    private float m_floatDistance;
    [SerializeField]
    private float m_floatRaycastSpreadAngle;
    [SerializeField]
    private float m_fallRate;
    [SerializeField]
    private float m_liftRate;
    [SerializeField]
    private float m_startMaxSpeedRate;
    [SerializeField]
    private float m_maxSpeed; // m/s
    [SerializeField, Range(0f, 4f)]
    private float m_acceleration; // time to get 0 to max speed or max speed to 0
    [SerializeField]
    private float m_turnPower;
    [SerializeField]
    private float m_minTurnRate;
    [SerializeField]
    private float m_maxTurnRate; // degrees/s
    [SerializeField]
    private float m_rotationLerpRate;
    [SerializeField]
    private float m_joystickDeadzone;
    [SerializeField]
    private int m_playerColor;
    // current percent of max speed
    
    public float m_currentSpeedRate = 0f;
    public float m_currentRotateRate = 0f;

    private Vector3 m_groundPoint = Vector3.down;
    private Vector3 m_downVector = Vector3.down;
    private float m_distanceToGround;

    private InputFrame m_input;
    private Rigidbody m_rb;
    private TrailRenderer m_trail;

    public float GetCurrentSpeedRate()
    {
        return m_currentSpeedRate;
    }
    private void Awake()
    {
        CurrentMaxSpeedRate = m_startMaxSpeedRate;
        m_rb = GetComponent<Rigidbody>();
        m_trail = GetComponent<TrailRenderer>();
        if (PlayerVariableHolder.instance != null)
            PlayerNumber = PlayerVariableHolder.instance.colorVsPlayer[m_playerColor - 1] + 1;
        else
        {
            Debug.Log("Playervariableholder is null");
        }
        m_input = InputFrame.FetchInput(PlayerNumber);
        m_groundPoint = transform.position - Vector3.up;
    }

    private void Update()
    {
        if (!GetComponent<Trail>().dead)
        {

            HandleInput();
            UpdateSpeed();
            UpdateDownVectorInterp();
        }
        else
        {
            m_rb.velocity = Vector3.zero;
        }
    }
    private void FixedUpdate()
    {
        if (!GetComponent<Trail>().dead)
        {

            UpdateRotation();
            UpdateMovement();
            HandleGrounding();
            AddScore();
        }
        else
        {
            m_rb.velocity = Vector3.zero;
        }
    }
    private void AddScore()
    {
        ScoreManager.instance.AddScore(PlayerNumber, Mathf.RoundToInt(m_currentSpeedRate * m_maxSpeed));
    }
    private void UpdateRotation()
    {
        float scaledTurnPower = m_turnPower * (1 + m_currentSpeedRate / 10);
        m_rb.AddTorque(transform.up * m_currentRotateRate * scaledTurnPower * Time.fixedDeltaTime, ForceMode.VelocityChange);

        float currentMaxTurnRate = m_minTurnRate + (m_maxTurnRate - m_minTurnRate) * (1 - m_currentSpeedRate / CurrentMaxSpeedRate);

        Vector3 degPerSec = m_rb.angularVelocity * Mathf.Rad2Deg;
        if (degPerSec.sqrMagnitude > currentMaxTurnRate * currentMaxTurnRate)
        {
            degPerSec = degPerSec.normalized * currentMaxTurnRate;
        }
        m_rb.angularVelocity = degPerSec * Mathf.Deg2Rad;

        //m_rb.MoveRotation(transform.rotation * Quaternion.AngleAxis(m_input.MoveVector.x * m_currentRotateRate * m_turnRate * Time.fixedDeltaTime, Vector3.up));
    }

    private void UpdateMovement()
    {
        m_rb.velocity = transform.forward * m_currentSpeedRate * m_maxSpeed;
    }

    private void HandleGrounding()
    {
        Vector3 targetPosition = m_groundPoint - m_downVector * m_floatDistance;
        float delta = Mathf.Abs(m_distanceToGround - m_floatDistance);

        m_rb.MovePosition(Vector3.Lerp(m_rb.position, targetPosition, m_rotationLerpRate * Time.fixedDeltaTime));

        /*float scaler = Mathf.Abs((m_distanceToGround - m_floatDistance) / m_floatDistance);
        if (m_distanceToGround > m_floatDistance)
        {
            //Debug.Log("Falling : " + scaler);
            m_rb.AddForce(-transform.up * m_fallRate * m_rb.mass * Mathf.Pow((1 + scaler), 2), ForceMode.Acceleration);
        }
        else if(m_distanceToGround < m_floatDistance)
        {
            //Debug.Log("Lifting : " + scaler);
            m_rb.AddForce(transform.up * m_liftRate * m_rb.mass * Mathf.Pow((1 + scaler), 2), ForceMode.Acceleration);
        }*/
    }

    private void HandleInput()
    {
        m_input = InputFrame.FetchInput(PlayerNumber);
        
        //deadzone for joystick x input
        if (Mathf.Abs(m_input.MoveVector.x) < m_joystickDeadzone)
            m_input.MoveVector.x = 0f;
        //deadzone for joystick y input + bottom out at 0 (no backwards movement)
        if (m_input.MoveVector.y < m_joystickDeadzone)
            m_input.MoveVector.y = 0f;
    }

    private void UpdateSpeed()
    {
        //float lerp = (m_input.MoveVector.y - m_currentSpeedRate / CurrentMaxSpeedRate) * Time.deltaTime / m_acceleration;
        //m_currentSpeedRate += lerp;
        m_currentSpeedRate = Mathf.Lerp(m_currentSpeedRate, m_input.MoveVector.y * CurrentMaxSpeedRate, Time.deltaTime / m_acceleration);

        m_currentRotateRate = Mathf.Lerp(m_currentRotateRate, m_input.MoveVector.x, m_rotationLerpRate * Time.deltaTime);
        m_currentRotateRate = Mathf.Clamp(m_currentRotateRate, -1f, 1f);
    }

    private void UpdateDownVectorInterp()
    {
        RaycastHit hit;
        Vector3 predictedPosition = transform.position + transform.forward * m_currentSpeedRate / CurrentMaxSpeedRate * m_maxSpeed;
        Vector3 predictedNormal = Vector3.up;

        Debug.DrawRay(transform.position, transform.forward * m_currentSpeedRate / CurrentMaxSpeedRate * m_maxSpeed * Time.fixedDeltaTime, Color.yellow, 1f, false);
        //raycast to the predicted next fixedupdate position
        if (!Physics.Raycast(transform.position, transform.forward, out hit, m_currentSpeedRate / CurrentMaxSpeedRate * m_maxSpeed * Time.fixedDeltaTime))
        {
            //Debug.Log("Failed forward");
            //Debug.DrawRay(transform.position + transform.forward * m_currentSpeedRate * m_maxSpeed * Time.fixedDeltaTime, -transform.up * 10, Color.green, 1f, false);
            //try raycasting down from that predicted position if no hit
            if (!Physics.Raycast(transform.position + transform.forward * m_currentSpeedRate / CurrentMaxSpeedRate * m_maxSpeed * Time.fixedDeltaTime, m_downVector, out hit, 100f))
            {
                return;
            }
        }

        //Interpolate the normal from the vert normals of the triangle we hit
        MeshCollider mc = hit.collider as MeshCollider;
        if (mc == null || mc.sharedMesh == null)
            return;

        Mesh m = mc.sharedMesh;
        Vector3[] normals = m.normals;
        int[] tris = m.triangles;

        Vector3 n0 = normals[tris[hit.triangleIndex * 3 + 0]];
        Vector3 n1 = normals[tris[hit.triangleIndex * 3 + 1]];
        Vector3 n2 = normals[tris[hit.triangleIndex * 3 + 2]];

        Vector3 baryCenter = hit.barycentricCoordinate;
        Vector3 interpolatedNormal = n0 * baryCenter.x + n1 * baryCenter.y + n2 * baryCenter.z;

        predictedPosition = hit.point;
        predictedNormal = hit.collider.transform.TransformDirection(interpolatedNormal.normalized);

        m_groundPoint = predictedPosition;
        m_downVector = -predictedNormal;
        Debug.DrawRay(predictedPosition, predictedNormal, Color.magenta, 1f, false);
        m_rb.rotation = Quaternion.Slerp(m_rb.rotation, Quaternion.FromToRotation(transform.up, -m_downVector) * m_rb.rotation, (m_rotationLerpRate * (1 + m_currentSpeedRate / CurrentMaxSpeedRate)) * Time.deltaTime);
    }

    private void UpdateDownVector()
    {
        RaycastHit hit;
        Vector3 avg = Vector3.zero;
        Vector3 ray = -transform.up;
        Debug.DrawRay(transform.position, ray * 5, Color.magenta, 5 * Time.deltaTime, false);

        if(Physics.Raycast(transform.position, ray, out hit, 100f))
        {
            avg -= hit.normal;
            Debug.DrawRay(hit.point, hit.normal * 2, Color.magenta);
            m_distanceToGround = hit.distance;
            m_groundPoint = hit.point;
        }

        ray = Quaternion.AngleAxis(m_floatRaycastSpreadAngle, transform.right) * -transform.up;
        Debug.DrawRay(transform.position, ray * 5, Color.magenta, 5 * Time.deltaTime, false);

        if (Physics.Raycast(transform.position, ray, out hit, 100f))
        {
            avg -= hit.normal;
            Debug.DrawRay(hit.point, hit.normal * 2, Color.magenta);
        }

        ray = Quaternion.AngleAxis(-m_floatRaycastSpreadAngle, transform.right) * -transform.up;
        Debug.DrawRay(transform.position, ray * 5, Color.magenta, 5 * Time.deltaTime, false);

        if (Physics.Raycast(transform.position, ray, out hit, 100f))
        {
            avg -= hit.normal;
            Debug.DrawRay(hit.point, hit.normal * 2, Color.magenta);
        }

        ray = Quaternion.AngleAxis(m_floatRaycastSpreadAngle, transform.forward) * -transform.up;
        Debug.DrawRay(transform.position, ray * 5, Color.magenta, 5 * Time.deltaTime, false);

        if (Physics.Raycast(transform.position, ray, out hit, 100f))
        {
            avg -= hit.normal;
            Debug.DrawRay(hit.point, hit.normal * 2, Color.magenta);
        }

        ray = Quaternion.AngleAxis(-m_floatRaycastSpreadAngle, transform.forward) * -transform.up;
        Debug.DrawRay(transform.position, ray * 5, Color.magenta, 5 * Time.deltaTime, false);

        if (Physics.Raycast(transform.position, ray, out hit, 100f))
        {
            avg -= hit.normal;
            Debug.DrawRay(hit.point, hit.normal * 2, Color.magenta);
        }

        avg /= 5;
        float dot = Vector3.Dot(avg, m_downVector);

        if(dot < 0.98)
        {
            m_downVector = Vector3.Lerp(m_downVector, avg, m_rotationLerpRate * Time.deltaTime);
        }
        else
        {
            m_downVector = Vector3.Lerp(m_downVector, avg, 2 * m_rotationLerpRate * Time.deltaTime);
        }

        float delta = Mathf.Abs(m_distanceToGround - m_floatDistance);

        m_rb.rotation = Quaternion.Slerp(m_rb.rotation, Quaternion.FromToRotation(transform.up, -m_downVector) * m_rb.rotation, (m_rotationLerpRate * (1 + m_currentSpeedRate / CurrentMaxSpeedRate)) * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        m_currentSpeedRate = 0f;
        Vector3 normal = (transform.position - collision.GetContact(0).point).normalized;
        Debug.DrawRay(collision.GetContact(0).point, normal * 2, Color.magenta, 0.5f);
        m_rb.AddForce(normal * m_maxSpeed * m_maxSpeed * 10);
    }
}
