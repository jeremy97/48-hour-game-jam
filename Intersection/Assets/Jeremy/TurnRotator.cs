﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnRotator : MonoBehaviour
{
    [SerializeField]
    private PlayerController m_target;
    [SerializeField]
    private float m_turnAngle;
    [SerializeField]
    private float m_lerpRate;

    private Quaternion m_baseRotation;

    private void Awake()
    {
        if(m_target == null)
            m_target = transform.parent.GetComponent<PlayerVisual>().m_target.GetComponent<PlayerController>();

        m_baseRotation = transform.localRotation;
    }

    private void Update()
    {
        Quaternion targetRot = Quaternion.AngleAxis(m_turnAngle * m_target.m_currentRotateRate, transform.forward) * m_baseRotation;
        transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRot, Time.deltaTime * m_lerpRate);
    }
}
