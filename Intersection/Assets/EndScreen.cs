﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class EndScreen : MonoBehaviour
{
    [SerializeField] TMP_Text player1;
    [SerializeField] TMP_Text player2;
    [SerializeField] TMP_Text player3;

    // Start is called before the first frame update
    void Awake()
    {
        player1.text =" " + ScoreManager.instance.p1Score.ToString();
        player2.text =" " + ScoreManager.instance.p2Score.ToString();
        player3.text =" " +ScoreManager.instance.p3Score.ToString();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
