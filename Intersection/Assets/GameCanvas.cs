﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameCanvas : MonoBehaviour
{

    [SerializeField] private float m_FirstOffset;
    [SerializeField] private float m_SecondOffset;

    // Start is called before the first frame update
    void Awake()
    {

        RectTransform rect = GetComponent<RectTransform>();


        GameObject temp = new GameObject();
        temp.transform.parent = gameObject.transform;
        Image tempImage = temp.AddComponent<Image>();
        tempImage.color = Color.black;

        GameObject temp2 = new GameObject();
        temp2.transform.parent = gameObject.transform;
        Image tempImage2 = temp2.AddComponent<Image>();
        tempImage2.color = Color.black;



        temp.transform.localScale = new Vector3(.25f, rect.rect.height, 0);
        temp2.transform.localScale = new Vector3(.25f, rect.rect.height, 0);

        temp.transform.position = new Vector3((rect.rect.width / 3) + m_FirstOffset, 0, rect.rect.height / 2);
        temp2.transform.position = new Vector3(((rect.rect.width / 3) + m_SecondOffset) * 2, 0, rect.rect.height / 2);



    }

    // Update is called once per frame
    void Update()
    {

    }
}
