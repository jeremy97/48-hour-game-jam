﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ScoreManager : MonoBehaviour
{
    [SerializeField] private TMP_Text p1ScoreT, p2ScoreT, p3ScoreT;
    public int p1Score, p2Score, p3Score = 0;
    public static ScoreManager instance;
    // Start is called before the first frame update
    void Start()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    public void AddScore(int player, int scoreToAdd)
    {
        if (player == 1)
        {
            p1Score += scoreToAdd;
        }
        else if (player == 2)
        {
            p2Score += scoreToAdd;

        }
        else if(player == 3)
        {
            p3Score += scoreToAdd;

        }
    }

    // Update is called once per frame
    void Update()
    {
        p1ScoreT.text ="Score: " + p1Score.ToString();
        p2ScoreT.text = "Score: " + p2Score.ToString();
        p3ScoreT.text = "Score: " + p3Score.ToString();

    }
}
