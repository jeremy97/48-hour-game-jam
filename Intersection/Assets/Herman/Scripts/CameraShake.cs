﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{

    [SerializeField] private float m_ShakeAmount = 0.7f;
    [SerializeField] private float m_DecreaseFactor = 1.0f;
    private float m_ShakeDuration;
    public CameraFade myCamFade;


    public void SetDuration(float aDuration)
    {
        m_ShakeDuration = aDuration;
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        if (m_ShakeDuration > 0)
        {
            transform.position = transform.parent.position + Random.insideUnitSphere * m_ShakeAmount;

            m_ShakeDuration -= Time.deltaTime * m_DecreaseFactor;
        }
        else
        {
            m_ShakeDuration = 0;
            transform.position = transform.parent.position;
        }
    }
}
