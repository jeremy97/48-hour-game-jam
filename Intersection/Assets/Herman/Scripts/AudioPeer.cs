﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPeer : MonoBehaviour
{



    public static AudioPeer instance;


    private AudioSource m_AudioSource;

    private float[] m_Samples = new float[512];


    [SerializeField] private float m_Decrease;
    [SerializeField] private float m_Increase;

    private float[] m_FrequencyBands = new float[8];


    private float[] m_BandBuffer = new float[8];
    public float[] m_BufferDecrease = new float[8];


    private float[] m_FreqBandHighest = new float[8];

    private float[] m_Audioband = new float[8];
    private float[] m_AudioBandBuffer = new float[8];



    private float m_Average = 0;

    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        m_AudioSource = GetComponent<AudioSource>();

    }


    private void Update()
    {
        GetSpectrumAudioSource();
        MakeFrequencyBands();
        ProcessBandBuffer();
        CreateAudioBands();
    }



    public float GetAverage()
    {
        return m_Average;
    }


    public float[] GetAudioBands()
    {
        return m_Audioband;
    }

    public void GetSpectrumAudioSource()
    {
        m_AudioSource.GetSpectrumData(m_Samples, 0, FFTWindow.Blackman);
    }


    public void MakeFrequencyBands()
    {
        int count = 0;
        for (int i = 0; i < 8; i++)
        {
            float average = 0;

            int sampleCount = (int)Mathf.Pow(2, i) * 2;

            if (i == 7)
            {
                sampleCount += 2;
            }

            for (int j = 0; j < 8; j++)
            {

                average += m_Samples[count] * count + 1;
                count++;
            }
            average /= count;

            m_FrequencyBands[i] = average * 10;
        }
    }

    public void ProcessBandBuffer()
    {
        for (int i = 0; i < 8; i++)
        {
            if (m_FrequencyBands[i] > m_BandBuffer[i])
            {
                m_BandBuffer[i] = m_FrequencyBands[i];
                m_BufferDecrease[i] = m_Decrease;
            }
            if (m_FrequencyBands[i] < m_BandBuffer[i])
            {

                m_BandBuffer[i] -= m_BufferDecrease[i];
                m_BufferDecrease[i] *= m_Increase;

            }
        }
    }

    public void CreateAudioBands()
    {
        for (int i = 0; i < 8; i++)
        {
            if (m_FrequencyBands[i] > m_FreqBandHighest[i])
            {
                m_FreqBandHighest[i] = m_FrequencyBands[i];
            }
            m_Audioband[i] = (m_FrequencyBands[i] / m_FreqBandHighest[i]);
            m_AudioBandBuffer[i] = (m_BandBuffer[i] / m_FreqBandHighest[i]);
        }

        float total = 0;

        for (int i = 0; i < 8; i++)
        {
            total += m_Audioband[i];
        }

        float temp = total / 8;


        m_Average = temp;
    }
}
