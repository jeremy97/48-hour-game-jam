﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereScaler : MonoBehaviour
{


    private Vector3 m_StartScale;

    [SerializeField] private int m_Band;

    private float[] m_Bands = new float[8];
    private void Start()
    {
        m_StartScale = gameObject.transform.localScale;
    }

    private void Update()
    {
        m_Bands = AudioPeer.instance.GetAudioBands();
        transform.localScale = new Vector3(m_StartScale.x * m_Bands[m_Band], m_StartScale.y * m_Bands[m_Band], m_StartScale.y * m_Bands[m_Band]);
    }
}
