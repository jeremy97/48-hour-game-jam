﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleColorChanger : MonoBehaviour
{
    [SerializeField] private ParticleSystem m_ParticleSystem;
    private ParticleSystem.MainModule main;
    public void SetParticleColors(Color aColor, Color aSecondColor)
    {
        ParticleSystem.MainModule newMain = m_ParticleSystem.main;
        ParticleSystem.MinMaxGradient gradient = new ParticleSystem.MinMaxGradient(aColor, aSecondColor);
        gradient.mode = ParticleSystemGradientMode.TwoColors;
        newMain.startColor = gradient;
    }
}
