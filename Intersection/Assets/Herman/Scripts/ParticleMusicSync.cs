﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleMusicSync : MonoBehaviour
{
    public enum Range
    {
        RangeOne,
        RangeTwo,
        RangeThree
    }


    private Material m_Material;

    private ParticleSystem m_ParticleSystem;
    ParticleSystem.Particle[] m_Particles;

    private float m_Percentage;

    private Color m_Color;

    private Vector3 m_ParticleScale;

    private ParticleSystemRenderer m_ParticleSystemRenderer;


    [SerializeField] private Range m_Range;

    [SerializeField] private float m_StartScale;

    [SerializeField] private float m_ParticleScaleMultiplier;

    [SerializeField] private float m_Min;
    [SerializeField] private float m_Max;


    [SerializeField] private float m_MinMultiplier;
    [SerializeField] private float m_MaxMultiplier;


    float temporary = 0;

    private void Start()
    {
        Invoke("ScaleHelper", 1f);
        m_ParticleSystem = GetComponent<ParticleSystem>();
        m_ParticleSystemRenderer = GetComponent<ParticleSystemRenderer>();
        m_Material = m_ParticleSystemRenderer.material;
        m_Color = m_Material.color;
        m_Particles = new ParticleSystem.Particle[m_ParticleSystem.main.maxParticles];
    }

    public void ScaleHelper()
    {
        m_ParticleScale = m_Particles[0].GetCurrentSize3D(m_ParticleSystem);
    }

    private void Update()
    {
        if (m_Range == Range.RangeOne)
        {
            float temp = 0;

            temp = (AudioPeer.instance.GetAudioBands()[0] + AudioPeer.instance.GetAudioBands()[1] + AudioPeer.instance.GetAudioBands()[2] / 3);
            temp = Mathf.InverseLerp(m_Min, m_Max, temp);
            m_Percentage = temp;

        }
        else if (m_Range == Range.RangeTwo)
        {
            float temp = 0;

            temp = (AudioPeer.instance.GetAudioBands()[3] + AudioPeer.instance.GetAudioBands()[4] / 2);

            temp = Mathf.InverseLerp(m_Min, m_Max, temp);
            m_Percentage = temp;

        }
        else if (m_Range == Range.RangeThree)
        {
            float temp = 0;

            temp = (AudioPeer.instance.GetAudioBands()[5] + AudioPeer.instance.GetAudioBands()[6] + AudioPeer.instance.GetAudioBands()[7] / 3);
            temp = Mathf.InverseLerp(m_Min, m_Max, temp);
            m_Percentage = temp;

        }

        int livingParticles = m_ParticleSystem.GetParticles(m_Particles);
        m_ParticleSystemRenderer.material.EnableKeyword("_EMISSION");

        Color tempColor = m_Material.color;

        Color finalColor = tempColor * Mathf.LinearToGammaSpace(5 * m_Percentage);

        m_ParticleSystemRenderer.material.SetColor("_EmissionColor", finalColor);
        m_ParticleSystemRenderer.trailMaterial.SetColor("_EmissionColor", finalColor);
        DynamicGI.SetEmissive(m_ParticleSystemRenderer, m_Color * m_Percentage * 1000);
        
        ParticleSystem.MainModule main = m_ParticleSystem.main;
       // main.startSpeed = m_Percentage * 5;

        for (int i = 0; i < livingParticles; i++)
        {
            Vector3 temp = new Vector3(m_Percentage * Random.Range(m_MinMultiplier,m_MaxMultiplier), m_Percentage * Random.Range(m_MinMultiplier, m_MaxMultiplier), m_Percentage * Random.Range(m_MinMultiplier, m_MaxMultiplier)) + m_ParticleScale;
            m_Particles[i].startSize3D = temp;
        }
        m_ParticleSystem.SetParticles(m_Particles, livingParticles);
    }
}
