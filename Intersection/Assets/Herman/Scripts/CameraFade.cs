﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CameraFade : MonoBehaviour
{
    [SerializeField] private Image m_FadeImage;
    [SerializeField] private bool m_StartFading;
    [SerializeField] private bool m_DisableFadeObj;

    private bool m_CanFade = true;

    // singleton method for the game manager


    private void Awake()
    {

    }

    // Use this for initialization
    void Start()
    {
        if (m_StartFading)
        {
            FadeFrom(Color.black, 4, false);
        }
        else if (m_DisableFadeObj)
        {
            m_FadeImage.enabled = false;
        }
    }

    // fade from a full color to transparent
    public void FadeFrom(Color aColor, float aSpeed, bool aDelayed)
    {
        if (aDelayed & m_CanFade)
        {
            m_CanFade = false;
            StartCoroutine(FadeHelper(aSpeed));
            m_FadeImage.color = aColor;
            m_FadeImage.canvasRenderer.SetAlpha(1.0f);
            m_FadeImage.CrossFadeAlpha(0.0f, aSpeed, false);
        }
        else if (m_CanFade)
        {
            m_FadeImage.color = aColor;
            m_FadeImage.canvasRenderer.SetAlpha(1.0f);
            m_FadeImage.CrossFadeAlpha(0.0f, aSpeed, false);
        }
    }

    // fade to black 
    public void FadeTo(Color aColor, float aSpeed, bool aDelayed)
    {
        if (aDelayed && m_CanFade)
        {
            m_CanFade = false;
            StartCoroutine(FadeHelper(aSpeed));
            m_FadeImage.color = aColor;
            m_FadeImage.canvasRenderer.SetAlpha(0.0f);
            m_FadeImage.CrossFadeAlpha(1.0f, aSpeed, false);
        }
        else if (m_CanFade)
        {
            m_FadeImage.color = aColor;
            m_FadeImage.canvasRenderer.SetAlpha(0.0f);
            m_FadeImage.CrossFadeAlpha(1.0f, aSpeed, false);
        }
    }

    // fades to black then to transparent
    public void FadeBoth(Color aColor, float aTime)
    {

        FadeTo(aColor, aTime, false);
        StartCoroutine(HermanIsDumb(aColor, aTime));
    }
    IEnumerator HermanIsDumb(Color aColor, float aTime)
    {
        yield return new WaitForSeconds(aTime);
        FadeFrom(aColor, aTime, true);
    }
    public IEnumerator FadeHelper(float aTime)
    {
        yield return new WaitForSeconds(aTime);
        m_CanFade = true;
    }

}
