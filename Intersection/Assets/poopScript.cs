﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class poopScript : MonoBehaviour
{
    [SerializeField] private float lifetime;
    public Trail.PoopType thisPoopType;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = Vector3.Lerp(transform.localScale, transform.localScale / 2, lifetime);
        if(transform.localScale.magnitude < .01f)
        {
            Destroy(gameObject);
        }
    }
}
