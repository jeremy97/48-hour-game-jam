﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawn : MonoBehaviour
{
    List<Transform> transforms = new List<Transform>();
    Dictionary<Transform, bool> spawnLocationDic = new Dictionary<Transform, bool>();
    [SerializeField] int minSpawnTime, maxSpawnTime;
    [SerializeField] GameObject pickup;
    public static PickupSpawn instance;
    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
        foreach (Transform t in GetComponentsInChildren<Transform>())
        {
            transforms.Add(t);
            spawnLocationDic[t] = false;
        }
        StartCoroutine(Spawn());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void DestroyPick(Transform trans)
    {
        spawnLocationDic[trans] = false;
    }
    IEnumerator Spawn()
    {
        while (true)
        {
            int rand = Random.Range(minSpawnTime, maxSpawnTime + 1);
            int rand2 = Random.Range(0, transforms.Count);
            while (spawnLocationDic[transforms[rand2]])
            {
                yield return new WaitForEndOfFrame();
                rand2 = Random.Range(0, transforms.Count);
            }
            yield return new WaitForSeconds(rand);
            Instantiate(pickup, transforms[rand2]);
            spawnLocationDic[transforms[rand2]] = true;
        }
    }
}
