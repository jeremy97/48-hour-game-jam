﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shaderTest : MonoBehaviour
{

    public Material shader;
    public float changeSpeedX;
    public float changeSpeedY;
    public bool wasHit = false;
    public float distortSpeed = 0.5f;
    public bool cool = false;
    bool sweet = false;
    float distortion = 0;
    public float linger = 1;
    public float lingerSpeed = 1;
    float dummy;
    float more = 0;
    public float crazyChange = 1;
    bool joke = false;
    float temp;
    float lossStatus;
    public bool isBad = false;
    public PlayerController playerController;
    public float threshold;
    public cameraNoise camNoi;
    // Start is called before the first frame update
    void Start()
    {
     dummy = linger;

    }

    // Update is called once per frame
    void Update()
    {
        changeSpeedY = playerController.m_currentSpeedRate;


        if (changeSpeedY < threshold)
        {
            isBad = true;
        }
        else
        {
            isBad = false;
        }

        if (!wasHit)
        {
            if (isBad)
            {
                distortion = (threshold - changeSpeedY) * 0.075f;
                camNoi.intesity = Mathf.Clamp(distortion, 0, .75f);
            }
        }
        if (wasHit)
        {
            shader.SetFloat("_EmissionGlow", 5);

            if (!joke)
            {
                temp = distortion;
                joke = true;
            }
            if (!cool)
            {
                distortion += distortSpeed * Time.deltaTime;
            }
            else
            {
                distortion -= distortSpeed * Time.deltaTime;
                if (distortion <= temp)
                {
                    cool = false;
                    wasHit = false;
                    joke = false;
                    distortion = temp;
                    shader.SetFloat("_EmissionGlow", 10);

                }
            }


            shader.SetFloat("_DistortState", more);

            if (distortion >= 1)
            {
                distortion = 1;
                linger -= lingerSpeed * Time.deltaTime;

                if (linger <= 0)
                {
                    linger = dummy;
                   
                    cool = true;
                }
            }
            if (!sweet)
            {
                more += crazyChange * Time.deltaTime;
            }
            else
            {
                more -= crazyChange * Time.deltaTime;
                if (more <= 0)
                {
                    sweet = false;
                    more = 0;
                }
            }
            if (more >= 1)
            {
                sweet = true;
            }
        }
        shader.SetFloat("_ScrollSpeedX", changeSpeedX);
        shader.SetFloat("_ScrollSpeedY", changeSpeedY);

        if (isBad || wasHit)
        {
            shader.SetFloat("_VertState", distortion);
            shader.SetFloat("_PlayerState", distortion);
            camNoi.intesity = Mathf.Clamp(distortion * 2, 0, .75f);

        }
        else
        {
            shader.SetFloat("_VertState", 0);
            shader.SetFloat("_PlayerState", 0);
            camNoi.intesity = 0;

        }

        //shader.SetFloat("ScrollSpeedX", Mathf.Sin(changeSpeedX * Time.time));

        //shader.SetFloat("ScrollSpeedY", Mathf.Sin(changeSpeedY * Time.time));


    }
}
