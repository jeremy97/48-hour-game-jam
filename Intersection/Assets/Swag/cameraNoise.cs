﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraNoise : MonoBehaviour
{
    public float intesity;
    public Material mat;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(intesity < 0)
        {
            intesity = 0;
        }
        if(intesity > 0.75)
        {
            intesity = 0.75f;
        }
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        mat.SetFloat("_Value", intesity);
        Graphics.Blit(source, destination, mat);
    }
}
