﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelBlendScript : MonoBehaviour
{
    public Material blendMat;
    public float changeSpeed;
    float change;
    bool test = false;
    public float linger;
    public float lingerSpeed;
    float dummy;
    public float fun;

    private void Start()
    {
        dummy = linger;
    }
    // Update is called once per frame
    void Update()
    {
        if (!test)
        {
            change += changeSpeed * Time.deltaTime;
        }
        else
        {
            change -= changeSpeed * Time.deltaTime;
            if (change <= 0)
            {
                change = 0;

                linger -= lingerSpeed * Time.deltaTime;
                if (linger <= 0)
                {
                    linger = dummy;

                    test = false;
                }
            }
        }

        if (change >= 1)
        {
            change = 1;

            linger -= lingerSpeed * Time.deltaTime;

            if (linger <= 0)
            {
                linger = dummy + fun;

                test = true;
            }

        }
        //Calling the _Value float in the blend shader and changing its value over time so it will cange automatically
        blendMat.SetFloat("_Value", Mathf.Sin(change));
    }
}
