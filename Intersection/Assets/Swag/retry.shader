﻿Shader "Custom/retry"
{
    Properties
    {
        _Color ("Main Color", Color) = (1,1,1,1)
		_StaticColor("Static Color", Color) = (1,1,1,1)

        _MainTex ("Albedo (RGB)", 2D) = "white" {}
	_SecondTex("Second Testure", 2D) = "white" {}
	_PlayerState("Player State", Range(0, 1)) = 0
	_StaticState("stat  State", Range(0, 1)) = 0
		_Amplitude("Amplitude", Float) = 10
		_Wavelength("Wavelength", Float) = 10
		_Speed("Speed", Float) = 1
		_VertState("Vert State", Range(0,1)) = 1
		_DistortState("Distortion State", Range(0, 1)) = 1
		_ScrollSpeedX("Scroll Speed X", Float) = 1
		_ScrollSpeedY("Scroll Speed Y", Float) = 1
		_StaticScrollSpeedX("Static Scroll Speed X", Float) = 1
		_StaticScrollSpeedY("Static Scroll Speed Y", Float) = 1
		_ScrollSpeedX2("X Resistance", Float) = 1
		_ScrollSpeedY2("Y Resistance", Float) = 1
			_EmissionGlow("Emission Intensity", Range(0, 10)) = 1
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
	sampler2D _SecondTex;

        struct Input
        {
            float2 uv_MainTex;
			float2 uv_SecondTex;
			float4 screenPos;
        };
		float _DistortState;
		float _VertState;
		float _Amplitude;
		float _Wavelength;
		float _Speed;
		float _StaticState;
		float _PlayerState;
		float _EmissionGlow;
		float _ScrollSpeedX;
		float _ScrollSpeedY;
		float _StaticScrollSpeedX;
		float _StaticScrollSpeedY;
		float _ScrollSpeedX2;
		float _ScrollSpeedY2;
        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
		fixed4 _StaticColor;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)
			
			


		void vert(inout appdata_full vertexData)
		{
			float3 p = vertexData.vertex.xyz;
			float3 i = vertexData.vertex.xyz;
			float3 l = vertexData.vertex.xyz;
			float3 u = vertexData.vertex.xyz;

			float k = 2 * UNITY_PI / _Wavelength;
			p.z = _Amplitude * sin(k * (p.y - _Speed * _Time.y));
			i.x = _Amplitude * sin(k * (i.z - _Speed * _Time.y));
			l.x = _Amplitude * sin(k * (l.x - _Speed * _Time.y));

			/*
			if (_VertState == 3)
			{
				vertexData.vertex.xyz = p;
			}
			else if (_VertState == 4)
			{
				vertexData.vertex.xyz = i;
			}
			else if (_VertState == 5)
			{
				vertexData.vertex.xyz = l;
			}
			else
			{
				vertexData.vertex.xyz = u;

			}
			*/
			float3 q = lerp(p, i, _DistortState);
			vertexData.vertex.xyz = lerp(u, q, _VertState);
		}

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			float4 staticTex = tex2D(_SecondTex, IN.screenPos);
			fixed2 mainScroll = IN.uv_MainTex;
			fixed2 staticScroll = staticTex;
			fixed2 staticScroll2 = staticTex;



			fixed scrollValueX = _ScrollSpeedX * 10 * _Time;
			fixed scrollValueY = _ScrollSpeedY * 10 * _Time;
			fixed statiScrollValueX = _StaticScrollSpeedX * 50 * _Time;
			fixed staticScrollValueY = _StaticScrollSpeedY * 50 * _Time;
			fixed staticScrollValueX2 = _ScrollSpeedX2 * 50 * _Time;
			fixed staticScrollValueY2 = _ScrollSpeedY2 * 50 * _Time;




			mainScroll += fixed2(scrollValueX, scrollValueY);
			staticScroll += fixed2(statiScrollValueX, staticScrollValueY);
			staticScroll2 -= fixed2(staticScrollValueX2, staticScrollValueY2);


			fixed2 scroll = IN.uv_MainTex + mainScroll;
			fixed2 statics = (IN.screenPos) + staticScroll;
			fixed stats = (IN.screenPos)  + staticScroll2;

			float4 stat = tex2D(_SecondTex, statics);
			float4 stati = tex2D(_SecondTex, stats);

			float4 staticTri = lerp(stat, stati, _StaticState) * _StaticColor;

			fixed4 g = tex2D(_MainTex, scroll) * _Color;
			//fixed4 r = static

            // Albedo comes from a texture tinted by color
            //fixed4 c = lerp(tex2D (_MainTex, scroll), staticTri, _PlayerState) * _Color;
			fixed4 c = lerp(g, staticTri, _PlayerState);
            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
			o.Emission = _EmissionGlow * c.rgb;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
