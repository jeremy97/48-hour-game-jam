﻿Shader "Custom/glassOrbShader"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_WaterFogColor("Water Fog Color", Color) = (0, 0, 0, 0)
		_WaterFogDensity("WaterFogDensity", Range(0, 2)) = 0.1
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_BumpMap("BumpMap", 2D) = "bump" {}
		_SecondBump("SecondBump", 2D) = "bump" {}
		_Value("Image Dominance", Range(0,1)) = 0.5
		_RefractionStrength("Refraction Strength", Range(0,1)) = 1
			//_Amplitude("Amplitude", Float) = 1
			//	_Steepness("Steepness", Range(0,1)) = 0.5
			//_Wavelength("Wavelength", Float) = 10


		_ScrollSpeedX("X Direction", Range(-5,5)) = 1
		_ScrollSpeedY("Y Direction", Range(-5,5)) = 1
		_ScrollSpeedX2("X Resistance", Range(-5,5)) = 1
		_ScrollSpeedY2("Y Resistance", Range(-5,5)) = 1
	}
		SubShader
		{
			Tags {"Queue" = "Transparent" "RenderType" = "Transparent" }
			LOD 200

			GrabPass{"_WaterBackground"}

			//ZWrite off

	CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma surface surf Standard alpha finalColor:ResetAlpha addshadow

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0

			sampler2D _MainTex;

			struct Input
			{
				float2 uv_MainTex;
				float2 uv_BumpMap;
				float2 uv_SecondBump;
				float4 screenPos;
			};

			half _Glossiness;
			half _Metallic;
			fixed4 _Color;
			sampler2D _BumpMap;
			sampler2D _SecondBump;
			sampler2D _CameraDepthTexture;
			float4 _CameraDepthTexture_TexelSize;
			sampler2D _WaterBackground;
			float3 _WaterFogColor;
			float _WaterFogDensity;
			float _Value;
			float _WaveChange;
			//float _Amplitude;
			//float _Steepness;
			//float _Wavelength;
			float _WaveSpeed;
			//float2 _Direction;
			float4 _WaveA;
			float4 _WaveB;
			float4 _WaveC;
			fixed _ScrollSpeedX;
			fixed _ScrollSpeedY;
			fixed _ScrollSpeedX2;
			fixed _ScrollSpeedY2;

			
			float _RefractionStrength;

			float3 ColorBelowWater(float4 screenPos, float3 tangentSpaceNormal)
			{
				float2 uvOffset = tangentSpaceNormal.xy * _RefractionStrength;
				uvOffset.y *= _CameraDepthTexture_TexelSize.z * abs(_CameraDepthTexture_TexelSize.y);
				float2 uv = (screenPos.xy + uvOffset) / screenPos.w;
				#if UNITY_UV_STARTS_AT_TOP
				if (_CameraDepthTexture_TexelSize.y < 0)
				{
					uv.y = 1 - uv.y;
				}
	#endif
				float backgroundDepth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, uv));
				float surfaceDepth = UNITY_Z_0_FAR_FROM_CLIPSPACE(screenPos.z);
				float depthDifference = backgroundDepth - surfaceDepth;
				float3 backgroundColor = tex2D(_WaterBackground, uv).rgb;
				float fogFactor = exp2(-_WaterFogDensity * depthDifference);
				return lerp(_WaterFogColor, backgroundColor, fogFactor);
				//return backgroundColor;
				//return depthDifference/20;
			}

			void ResetAlpha(Input IN, SurfaceOutputStandard o, inout fixed4 color)
			{
				color.a = 1;
			}

			// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
			// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
			// #pragma instancing_options assumeuniformscaling
			UNITY_INSTANCING_BUFFER_START(Props)
				// put more per-instance properties here
			UNITY_INSTANCING_BUFFER_END(Props)

			void surf(Input IN, inout SurfaceOutputStandard o)
			{

				fixed2 scrollUV = IN.uv_BumpMap;
				fixed2 otherScroll = IN.uv_BumpMap;

				fixed scrollValueX = _ScrollSpeedX * _Time;
				fixed scrollValueY = _ScrollSpeedY * _Time;
				fixed scrollValueX2 = _ScrollSpeedX2 * _Time;
				fixed scrollValueY2 = _ScrollSpeedY2 * _Time;

				scrollUV += fixed2(scrollValueX, scrollValueY);
				otherScroll -= fixed2(scrollValueX2, scrollValueY2);



				fixed2 normUV = IN.uv_BumpMap + scrollUV;
				fixed2 secondNorm = IN.uv_BumpMap + otherScroll;

				float4 normPix = tex2D(_BumpMap, normUV);
				float4 otherPix = tex2D(_BumpMap, secondNorm);
				float4 blendPix = tex2D(_SecondBump, IN.uv_SecondBump);
				float3 n = UnpackNormal(normPix);
				float3 m = UnpackNormal(otherPix);
				float3 k = UnpackNormal(blendPix);

				float3 g = lerp(n, m, _Value);
				float3 l = lerp(g, k, _WaveChange);
				//half4 g = tex2D(_BumpMap, scrollUV);
				// Albedo comes from a texture tinted by color
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				// o.Albedo = c.rgb;
				 o.Albedo = c.rgb;

				 o.Normal = l.xyz;
				 // Metallic and smoothness come from slider variables
				 o.Metallic = _Metallic;
				 o.Smoothness = _Glossiness;
				 o.Alpha = c.a;

				 o.Emission = ColorBelowWater(IN.screenPos, o.Normal) * (1 - c.a);
			 }
			 ENDCG
		}
}
