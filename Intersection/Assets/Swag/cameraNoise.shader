﻿Shader "Custom/cameraNoise"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_SecondTex("Texture", 2D) = "white" {}
		_Value("Value", Range(0,1)) = 0
		_StaticState("Static State", Range(0,1)) = 0.5

			_StaticScrollSpeedX("Static Scroll Speed X", Float) = 1
		_StaticScrollSpeedY("Static Scroll Speed Y", Float) = 1
		_ScrollSpeedX2("X Resistance", Float) = 1
		_ScrollSpeedY2("Y Resistance", Float) = 1

    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
			sampler2D _SecondTex;
			float _StaticScrollSpeedX;
			float _StaticScrollSpeedY;
			float _ScrollSpeedX2;
			float _ScrollSpeedY2;
			float _Value;
			float _StaticState;

            fixed4 frag (v2f i) : SV_Target
            {
				float4 staticTex = tex2D(_SecondTex, i.uv);
			//fixed2 mainScroll = IN.uv_MainTex;
			fixed2 staticScroll = staticTex;
			fixed2 staticScroll2 = staticTex;



			fixed statiScrollValueX = _StaticScrollSpeedX * _Time;
			fixed staticScrollValueY = _StaticScrollSpeedY * _Time;
			fixed staticScrollValueX2 = _ScrollSpeedX2  * _Time;
			fixed staticScrollValueY2 = _ScrollSpeedY2  * _Time;

			staticScroll += fixed2(statiScrollValueX, staticScrollValueY);
			staticScroll2 -= fixed2(staticScrollValueX2, staticScrollValueY2);

			fixed2 statics = staticTex + staticScroll;
			fixed stats = staticTex + staticScroll2;

			float4 stat = tex2D(_SecondTex, statics);
			float4 stati = tex2D(_SecondTex, stats);

			float4 staticTri = lerp(stat, stati, _StaticState);

			//fixed4 g = tex2D(_MainTex, scroll);
			//fixed4 r = static

			// Albedo comes from a texture tinted by color
			//fixed4 c = lerp(tex2D (_MainTex, scroll), staticTri, _PlayerState) * _Color;
			//fixed4 c = lerp(g, staticTri, _PlayerState);
                fixed4 col = lerp(tex2D(_MainTex, i.uv), staticTri, _Value);
                // just invert the colors
                col.rgb = col.rgb;
                return col;
            }
            ENDCG
        }
    }
}
